## PART 1

- **Берем официальный Docker образ nginx с DockerHub**
![Docker pull](images/part1/docker_pull.png)


- **Проверяем наличие докер образа**

    ![Docker images](images/part1/docker_images.png)


- **Создаем котейнер на основе Docker образа nginx. Контейнер работает в фоновом режиме за счет флага -d.**
![Docker run -d](images/part1/docker_run.png)


- **Проверяем наличие запущенного контейнера nginx.**

    ![Docker ps](images/part1/docker_ps.png)


- **Получаем подробную информацию о контейнере в JSON формате. С помощью флага -s получаем размер контейнера.**

    ![Docker inspect -s](images/part1/docker_inspect.png)

    **Исходя из JSON вывода команды docker inspect мы получаем:**
1. Размер контейнера(SizeRootFs) + размер файловой системы контейнера(SizeRw). Информация предоставляется в байтах:

    ![Container size + file system size](images/part1/size_of_container.png)

2. Замапленные порты:
    
    ![Port_Bindings](images/part1/port_bindings.png)

3. IP контейнера:

    ![Container IP](images/part1/ip_address.png)


- **Останавливем nginx контейнер** 
  
    ![Docker stop](images/part1/docker_stop.png)


- **Проверяем, что контейнер остановлен**

    ![Docker ps](images/part1/docker_ps_check.png)


- **Создаем контейнер с замапленными портами 80 и 443**

  ![Docker ps](images/part1/port_mapping.png)


- **Проверяем замапленный 80 порт с помощью localhost:80**

  ![Docker ps](images/part1/mapping_check.png)


- **Перезапускаем контейнер**
  
    ![Docker ps](images/part1/docker_restart.png)


- **Проверяем, что контейнер перезапустился**

  ![Docker ps](images/part1/docker_restart_check.png)


- **Проверяем замапленные порты после перезапуска**

  ![Docker restart mapping check](images/part1/docker_restart_mapping_check.png)


## PART 2

- **Читаем конфиг файл nginx и создаем его на хост машине**

  ![Nginx conf check](images/part2/read_write_nginx.png)


- **Настраиваем nginx.conf для отдачи страницы статуса nginx, загружаем новый конфиг через docker cp и перезагружаем nginx**

  ![Nginx conf check](images/part2/push_new_nginx.png)


- **Проверяем, что по адресу localhost:80 имеется главная страница и по localhost:80/status отдается страница статуса**
1. localhost:80

    ![main page](images/part2/main_page.png)

2. localhost:80/status

    ![status page](images/part2/status_page.png)


- **Экспортируем файловую систему контейнера в container.tar**

    ![Docker export](images/part2/docker_export.png)


- **Останавливаем контейнер**

  ![Docker export](images/part2/docker_stop.png)


- **Удаляем образ не удаляя контейнеры**

  ![Docker export](images/part2/docker_rmi.png)


- **Удаляем остановленный контейнер**

  ![Docker export](images/part2/docker_rm.png)


- **Импортируем экспортированный образ (container.tar)**

  ![Docker export](images/part2/docker_import.png)


- **Создаем контейнер на основе нашего образа**

  ![Docker export](images/part2/docker_custom_run.png)


- **Проверяем, что после запуска контейнера страницы: localhost:80 и localhost:80/status работают**

1. localhost:80

  ![Dcoker export](images/part2/page_import_check.png)

2. localhost:80/status

   ![Dcoker export](images/part2/page_status_import_check.png)


## PART 3

- **Пишем простенький fcgi сервер, который будет отдавать Hello World на каждый звпрос от веб сервера.** 
  - gcc -o fcgi fcgi.c -I /opt/homebrew/Cellar/fcgi/2.4.2/include -L /opt/homebrew/Cellar/fcgi/2.4.2/lib -lfcgi
  
     ![Dcoker export](images/part3/fcgi_c.png)


- **Запускаем наше приложение через spawn-fcgi и устанавливаем порт 8080**
  - spawn-fcgi -p 8080 -n fcgi
  
    ![spawn-cgi](images/part3/spawn_fcgi.png)


- **Добавялем директиву server и location в конфиг nginx. Директива server будет слушать 81 порт и проксировать запросы на порт 8080.**

  ![nginx_conf](images/part3/nginx_conf.png)


- **Заходим в браузер и подключаемся по localhost:81**

  ![hello_world](images/part3/hello_world.png)

## PART 4

- **С помощью команды CMD запускаем fcgi сервер, spawn-fcgi на порту 8080 и nginx с выключенным фоновым режимом. С помощью команды COPY копируем конфиг nginx с хоста в контейнер**

  ![hello_world](images/part4/dockerfile_check.png)


- **Собираем докер образ с помощью docker build. Указываем имя и тег(версию) образа**

  ![hello_world](images/part4/docker_build.png)


- **Проверяем, что docker образ загрузился**

  ![hello_world](images/part4/docker_image.png)


- **Запускаем docker образ с замапленными портами 80 на 81 контейнера и маппим наш nginx конфиг**

  ![hello_world](images/part4/dcoker_run_mapping.png)


- **Провряем что на localhost:80 доступна написанная нами страница**

  ![hello_world](images/part4/page_check.png)


- **Добавляем в nginx.conf страницу статуса nginx**

  ![hello_world](images/part4/add_location.png)


- **Перезапускаем докер образ**

  ![hello_world](images/part4/container_restart.png)


- **Проверяем, что по адресу localhost:80/status отдается страница статуса**

  ![hello_world](images/part4/status_page_check.png)


## PART 5

- **Проверяем с помощью утилиты dockle безопасность нашего docker образа**

  ![hello_world](images/part5/dockle_check.png)
  - Видим одну ошибку. Она указывает на то, что работа в контейнере производится через root пользователя. Для фикса ошибки создаем пользователя и в docker образе указываем от какого пользователя будет идти работа контейнера(Команда USER).
  
    ![hello_world](images/part5/error_fix.png)


## PART 6

- **Поднимаем первый контейнер без маппинга портов**

  ![hello_world](images/part6/docker_fn_up.png)
  - Dockerfile
  
    ![hello_world](images/part6/dockerfile_first.png)


- Поднимаем чистый nginx сервер с маппингои 80 на 8080, а внутри проксируем все на 81.

  ![hello_world](images/part6/docker_second_up.png)
  - Dockerfile
  
    ![hello_world](images/part6/dockerfile_second.png)

  - nginx.conf
  
    ![hello_world](images/part6/nginx_second.png)
  
  - IP первого контейнера
  
    ![hello_world](images/part6/ip_of_the_first_contianer.png)
  
  - Проверяем, что все работает
  
    ![hello_world](images/part6/page_status_check.png)


- **Останавливаем все контейнеры**

  ![hello_world](images/part6/container_stop.png)


- **Собираем и запускаем проект с помощью docker-compose build и docker-compose up**
  ![hello_world](images/part6/build_up.png)
  - Docker-compose файл
  
    ![hello_world](images/part6/docker-compose_file.png)
  - nginx конфиг второго контейнра
  
    ![hello_world](images/part6/nginx_with_dns.png)


- **Проверяем, что все работает**

  ![hello_world](images/part6/docker-compose_page_check.png)